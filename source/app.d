module app;

import std.getopt;
import n.command.all;
import n.options;

void main(string[] args)
{
    auto result = getopt(args,
            "c|config", &options.configFile,
            "d|debug", &options.verbose
            );

    auto command = commandFactory!(
            HelpCommand,
            ListNotesCommand,
            AddNoteCommand
    )(args.length == 1 ? "list" : args[1]);

    command.execute();
}
