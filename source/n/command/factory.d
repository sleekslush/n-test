module n.command.factory;

import n.command.base;

BaseCommand commandFactory(DefaultCommand : BaseCommand, Commands...)(string name)
{
    foreach (Command; Commands)
    {
        static assert(is(Command : BaseCommand));

        if (Command.name == name)
        {
            return new Command();
        }
    }

    return new DefaultCommand();
}

private version(unittest)
{
    class TestCommand : BaseCommand
    {
        override void execute() {}
    }

    class DefaultCommand : TestCommand
    {
        static string name = "default";
    }

    class SpecificCommand1 : TestCommand
    {
        static string name = "specific1";
    }

    class SpecificCommand2 : TestCommand
    {
        static string name = "specific2";
    }

    template CommandFactoryTester(DC, Cs...)
    {
        void test(string name = null)
        {
            alias factory = commandFactory!(DC, Cs);

            assert(cast(DefaultCommand) factory("") !is null, "Unknown name should return default command");
            assert(cast(DefaultCommand) factory("default") !is null, "Default command should return when explicitly requested");

            foreach (C; Cs)
            {
                auto command = factory(C.name);
                assert(cast(C) command !is null, "A non-default command should return when found");
                assert(cast(DefaultCommand) command is null, "The default command should only be listed first");
            }
        }
    }
}

unittest
{
    CommandFactoryTester!DefaultCommand.test();
    CommandFactoryTester!(DefaultCommand, SpecificCommand1).test();
    CommandFactoryTester!(DefaultCommand, SpecificCommand1, SpecificCommand2).test();
}
