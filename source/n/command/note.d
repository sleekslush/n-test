module n.command.note;

import n.command.base;
import std.stdio;

class ListNotesCommand : BaseCommand
{
    static string name = "list";

    override void execute()
    {
        writeln(name);
    }
}

class AddNoteCommand : BaseCommand
{
    static string name = "add";

    override void execute()
    {
        writeln(name);
    }
}
