module n.command.base;

abstract class BaseCommand
{
    void execute();
}
