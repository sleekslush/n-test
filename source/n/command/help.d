module n.command.help;

import n.command.base;
import std.stdio;

class HelpCommand : BaseCommand
{
    static string name = "help";

    override void execute()
    {
        writeln(name);
    }
}
