module n.options;

struct Options
{
    string configFile = "~/.n/config";
    bool verbose;
}

static Options options;
